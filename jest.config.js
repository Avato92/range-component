module.exports = {
  roots: ["<rootDir>/src"],

  testEnvironment: "jest-environment-jsdom",

  testPathIgnorePatterns: ["/node_modules/", "/dist/", "/public/"],

  moduleFileExtensions: ["js", "jsx"],

  moduleNameMapper: {
    "\\.(scss)$": "identity-obj-proxy",
  },

  reporters: [
    "default",
    ["jest-junit", { outputDirectory: "test-reports/junit" }],
  ],
};
