import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Exercise1 from "./containers/Exercise1.jsx";
import Exercise2 from "./containers/Exercise2.jsx";

function AppRouter() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/exercise1" element={<Exercise1 />} />
        <Route exact path="/exercise2" element={<Exercise2 />} />
      </Routes>
    </Router>
  );
}

function Home() {
  return <h1>Welcome to the Home Page!</h1>;
}

export default AppRouter;
