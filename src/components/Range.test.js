import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Range from "./Range";

describe("Range component tests", () => {
  const setup = (props = {}) => {
    const {
      min = 0,
      max = 100,
      value = { min: 0, max: 100 },
      step = 5,
      onChange = jest.fn(),
    } = props;
    const { getByTestId } = render(
      <Range
        min={min}
        max={max}
        value={value}
        step={step}
        onChange={onChange}
      />
    );
    const minInput = getByTestId("min-input");
    const maxInput = getByTestId("max-input");

    return {
      minInput,
      maxInput,
      range: getByTestId("range"),
    };
  };

  test("renders with default values", () => {
    const { minInput, maxInput } = setup();
    expect(minInput.value).toBe("0");
    expect(maxInput.value).toBe("100");
  });

  test("updates minimum value", () => {
    const onChange = jest.fn();
    const { minInput } = setup({ onChange });
    fireEvent.change(minInput, { target: { value: 25 } });
    expect(onChange).toHaveBeenCalledWith({ min: 25, max: 100 });
  });

  test("updates maximum value", () => {
    const onChange = jest.fn();
    const { maxInput } = setup({ onChange });
    fireEvent.change(maxInput, { target: { value: 75 } });
    expect(onChange).toHaveBeenCalledWith({ min: 0, max: 75 });
  });

  test("updates minimum and maximum values", () => {
    const onChange = jest.fn();
    const { minInput, maxInput } = setup({ onChange });

    fireEvent.change(minInput, { target: { value: 25 } });
    expect(onChange).toHaveBeenCalledWith({ max: 100, min: 25 });

    fireEvent.change(maxInput, { target: { value: 75 } });
    expect(onChange).toHaveBeenCalledWith({ max: 75, min: 0 });
  });

  test("updates minimum value with step", () => {
    const onChange = jest.fn();
    const { minInput } = setup({ onChange, step: 10 });
    fireEvent.change(minInput, { target: { value: 25 } });
    expect(onChange).toHaveBeenCalledWith({ min: 25, max: 100 });
  });

  test("updates maximum value with step", () => {
    const onChange = jest.fn();
    const { maxInput } = setup({ onChange, step: 10 });
    fireEvent.change(maxInput, { target: { value: 75 } });
    expect(onChange).toHaveBeenCalledWith({ min: 0, max: 75 });
  });
});
