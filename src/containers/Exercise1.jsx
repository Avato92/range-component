import React, { useState, useEffect } from "react";
import Range from "../components/Range.jsx";
import axios from "axios";
import "./exercises.scss";

function Exercise1() {
  const [value, setValue] = useState({ min: 0, max: 100 });
  const [min, setMin] = useState(0);
  const [max, setMax] = useState(0);

  useEffect(() => {
    axios.get("http://demo2897569.mockable.io/").then((response) => {
      setMin(response.data.min);
      setMax(response.data.max);
    });
  }, []);

  return (
    <>
      <Range min={min} max={max} step={1} value={value} onChange={setValue} />
      <p>
        The min value is: <span>{value.min}</span>
      </p>
      <p>
        The max value is: <span>{value.max}</span>
      </p>
    </>
  );
}

export default Exercise1;
